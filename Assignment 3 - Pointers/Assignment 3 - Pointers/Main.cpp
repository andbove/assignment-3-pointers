
// Assignment 3 - Pointers
// Andrew Bovee


#include <iostream>
#include <conio.h>
#include "Main.h";

using namespace std;

// TODO: Implement the "SwapIntegers" function
void SwapIntegers(int *n1, int *n2)
{
	int point;
	point = *n1;
	*n1 = *n2;
	*n2 = point;
}
// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	(void)_getch();
	return 0;
}

